package com.base.xm.fingerprintdemo.module;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.base.xm.fingerprintdemo.R;
import com.base.xm.fingerprintdemo.utils.FingerprintHelper;
import com.base.xm.fingerprintdemo.utils.LocalSharedPreference;

/**
 * 登录页
 * （指纹验证登录的思路：
 * 1.一般首次登录需要用户通过账号和密码进行登录，登录成功后将登录密钥token存储于本地；
 * <p>
 * 2.登录成功后，进入设置页面去开启指纹登录功能，这里需要由后台接口来记录开启/关闭状态（也可以同时本地存储一下，
 * 本地存储的目的仅仅只为了下次登录时不需要再调用接口判断是否有开启指纹登录功能），当然每次开启时，需要先成功验证
 * 一次指纹，将加密后的token与指纹关联起来（在这里token会被存储于系统文件中，所以一般用户如果没有通过指纹成功验证是拿不到，
 * 但是手机被ROOT了，那么用户就有了最高权限，是可以被窃取到的，所以一般我们不要将指纹和账号密码或支付密码等重要信息关联起来）；
 * <p>
 * 3.每次重启APP时，需要通过接口来判断是否已经开启了指纹登录功能（如果本地有存储该状态值，则不需要再调用接口，
 * 这样APP启动登录页比较流程快速，不需要等待接口响应），如果已经开启则调取指纹登录的界面，否则调取普通登录的页面；
 * <p>
 * 4.当调起指纹登录界面时，先成功匹配指纹，然后从成功的回调方法中取得与指纹关联的token，最后通过token去发起新的登录接口请求）
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        FingerprintHelper.Callback {

    EditText et_username, et_password;
    Button btn_login;

    //指纹识别功能类
    FingerprintHelper helper;
    //本地数据存储工具类
    LocalSharedPreference sharedPreference;
    //对话框
    AlertDialog dialog;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        helper = FingerprintHelper.getIntance(this);
        helper.setCallback(this);
        sharedPreference = new LocalSharedPreference(this);

        //判断是否开启"指纹登录"开关
        if (sharedPreference.getInt(LocalSharedPreference.STATE)
                == LocalSharedPreference.OPEN) {
            //显示对话框
            showDialog();
            //设置指纹验证的目的是为了解密数据
            helper.setPurpose(KeyProperties.PURPOSE_DECRYPT);
            //开启指纹验证
            helper.authenticate();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login://普通登录
                login(et_username.getText().toString(), et_password.getText().toString());
                break;
        }
    }

    /**
     * 登录接口（根据用户名和密码进行匹配的登录接口）
     *
     * @param username
     * @param password
     */
    public void login(String username, String password) {
        //这里假设已经调用了登录接口，并成功返回登录密钥token
        String token = "token123";
        //本地存储token
        sharedPreference.putString(LocalSharedPreference.TOKEN, token);
        //跳转到主页
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    /**
     * 登录接口（让服务端提供一个根据token进行匹配的登录接口）
     *
     * @param token
     */
    public void login(String token) {
        //这里假设已经调用了登录接口，并成功返回新的密钥
        String newToken = "token456";
        //本地存储token
        sharedPreference.putString(LocalSharedPreference.TOKEN, newToken);
        //跳转到主页
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    /**
     * 创建并显示对话框
     */
    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("指纹认证");
        builder.setIcon(R.drawable.ic_fp_40px);
        builder.setMessage("指纹认证中.....");
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    /**
     * 关闭对话框
     */
    public void dissmissDialog() {
        dialog.dismiss();
        dialog = null;
    }

    @Override
    public void onHardwareNotSupported() {
        Toast.makeText(this, "手机硬件不支持指纹识别", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoPermission() {
        Toast.makeText(this, "没有权限", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNotEnrolledFingerprints() {
        Toast.makeText(this, "手机没有录入指纹", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onKeyguardSecure() {
        Toast.makeText(this, "手机已设置锁屏", Toast.LENGTH_SHORT).show();
    }

    /**
     * 认证成功
     *
     * @param value 解密后返回的token
     */
    @Override
    public void onAuthenticationSucceeded(String value) {
        Toast.makeText(this, "验证成功", Toast.LENGTH_SHORT).show();
        //关闭对话框
        dissmissDialog();
        //登录
        login(value);
    }

    /**
     * 指纹验证失败超过5次
     *
     * @param errorCode
     * @param errString
     */
    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        //对话框显示纹识别次数数过多的提示文案
        if (dialog != null)
            dialog.setMessage(errString);
    }

    /**
     * 验证出问题时的提示
     *
     * @param helpCode
     * @param helpString
     */
    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {

    }

    /**
     * 验证失败
     */
    @Override
    public void onAuthenticationFailed() {
        Toast.makeText(this, "验证失败，请再验证一次", Toast.LENGTH_SHORT).show();
    }

}
