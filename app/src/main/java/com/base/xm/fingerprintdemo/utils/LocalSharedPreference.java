package com.base.xm.fingerprintdemo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * 本地数据存储工具类
 */
public class LocalSharedPreference {
    //加密后的数据
    public static final String DATA_NAME = "data";
    public static final String IV_NAME = "iv";
    //登录密钥
    public static final String TOKEN = "token";
    //指纹登录开关的状态
    public static final String STATE = "state";
    public static final int OPEN = 1, CLOSE = 0;
    private SharedPreferences preferences;

    public LocalSharedPreference(Context context) {
        preferences = context.getSharedPreferences("Fingerprint", Activity.MODE_PRIVATE);
    }

    public String getString(String key) {
        return preferences.getString(key, "");
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public int getInt(String key) {
        return preferences.getInt(key, 0);
    }

    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }
}
