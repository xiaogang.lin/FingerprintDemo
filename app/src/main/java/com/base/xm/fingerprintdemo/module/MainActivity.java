package com.base.xm.fingerprintdemo.module;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.base.xm.fingerprintdemo.R;

/**
 * 主页
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_back, tv_setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_back = findViewById(R.id.tv_back);
        tv_back.setOnClickListener(this);
        tv_setting = findViewById(R.id.tv_setting);
        tv_setting.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.tv_setting://设置指纹登录
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    Toast.makeText(this, "您的系统版本太低，不支持指纹识别",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                //跳转到设置页面
                startActivity(new Intent(this, SettingActivity.class));
                break;
        }
    }
}
