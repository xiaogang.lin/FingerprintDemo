package com.base.xm.fingerprintdemo.module;

import android.app.AlertDialog;
import android.os.Build;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.base.xm.fingerprintdemo.R;
import com.base.xm.fingerprintdemo.utils.FingerprintHelper;
import com.base.xm.fingerprintdemo.utils.LocalSharedPreference;

/**
 * 设置页
 *
 */
public class SettingActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener,
        FingerprintHelper.Callback {
    SwitchCompat sc;

    //指纹识别工具类
    FingerprintHelper helper;
    //本地数据存储工具类
    LocalSharedPreference sharedPreference;
    //对话框
    AlertDialog dialog;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        sc = findViewById(R.id.sc);
        sc.setOnCheckedChangeListener(this);

        helper = FingerprintHelper.getIntance(this);
        helper.setCallback(this);
        sharedPreference = new LocalSharedPreference(this);

        //设置开关开启/关闭状态
        sc.setChecked(sharedPreference.getInt(LocalSharedPreference.STATE)
                == LocalSharedPreference.OPEN ? true : false);
    }

    /**
     * 创建并显示对话框
     */
    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("指纹认证");
        builder.setIcon(R.drawable.ic_fp_40px);
        builder.setMessage("指纹认证中.....");
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    /**
     * 关闭对话框
     */
    public void dissmissDialog() {
        dialog.dismiss();
        dialog = null;
    }

    /**
     * 开关事件
     *
     * @param buttonView
     * @param isChecked
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked && sharedPreference.getInt(LocalSharedPreference.STATE)
                == LocalSharedPreference.CLOSE) {
            //显示对话框
            showDialog();
            //设置指纹验证的目的是为了加密数据
            helper.setPurpose(KeyProperties.PURPOSE_ENCRYPT);
            //设置token为加密数据
            helper.setEncodeValue(sharedPreference.getString(LocalSharedPreference.TOKEN));
            //开启指纹验证
            helper.authenticate();
        } else if (!isChecked) {
            //保存关闭状态
            sharedPreference.putInt(LocalSharedPreference.STATE, LocalSharedPreference.CLOSE);
        }
    }

    @Override
    public void onHardwareNotSupported() {
        Toast.makeText(this, "手机硬件不支持指纹识别", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoPermission() {
        Toast.makeText(this, "没有权限", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNotEnrolledFingerprints() {
        Toast.makeText(this, "手机没有录入指纹", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onKeyguardSecure() {
        Toast.makeText(this, "手机已设置锁屏", Toast.LENGTH_SHORT).show();
    }

    /**
     * 指纹验证成功
     *
     * @param value 加密后的token值
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onAuthenticationSucceeded(String value) {
        Toast.makeText(this, "验证成功", Toast.LENGTH_SHORT).show();
        //隐藏对话框
        dissmissDialog();
        //保存开启状态
        sharedPreference.putInt(LocalSharedPreference.STATE, LocalSharedPreference.OPEN);
        //开启开关
        sc.setChecked(true);

    }

    /**
     * 指纹验证失败超过5次
     *
     * @param errorCode
     * @param errString
     */
    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        //关闭开关
        sc.setChecked(false);
        //对话框显示纹识别次数数过多的提示文案
        if (dialog != null)
            dialog.setMessage(errString);
    }

    /**
     * 验证出问题时的提示
     *
     * @param helpCode
     * @param helpString
     */
    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        //关闭开关
        sc.setChecked(false);
    }

    /**
     * 验证失败
     */
    @Override
    public void onAuthenticationFailed() {
        Toast.makeText(this, "验证失败，请再验证一次", Toast.LENGTH_SHORT).show();
        //关闭开关
        sc.setChecked(false);
    }
}
